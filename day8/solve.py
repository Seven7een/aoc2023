import math

def solve_a(input_file):

    with open(input_file, "r") as inputfile:

        lines = [x.strip() for x in inputfile.readlines()]

        input = lines[0]
        direction_map = {}
        for line in lines[2:]:
            # Line is AAA = (BBB, CCC)
            location = line.split("=")[0].strip()
            left = line.split("=")[1][2:5]
            right = line.split("=")[1][-4:-1]
            direction_map[location] = {"L": left, "R": right}

        curr_loc = "AAA"
        steps = 0

        while curr_loc != "ZZZ":
            curr_loc = direction_map[curr_loc][input[steps % len(input)]]
            steps += 1

        print(f"Steps: {steps}")

def solve_b(input_file):

    with open(input_file, "r") as inputfile:

        lines = [x.strip() for x in inputfile.readlines()]

        input = lines[0]
        direction_map = {}
        for line in lines[2:]:
            # Line is AAA = (BBB, CCC)
            location = line.split("=")[0].strip()
            left = line.split("=")[1][2:5]
            right = line.split("=")[1][-4:-1]
            direction_map[location] = {"L": left, "R": right}

        locs = [x for x in list(direction_map.keys()) if x[-1] == "A"]
        steps = 0

        first_solves = []

        for loc in locs:
            curr_loc = loc
            steps = 0
            while curr_loc[-1] != "Z":
                curr_loc = direction_map[curr_loc][input[steps % len(input)]]
                steps += 1
            print(f"Steps: {steps}")
            first_solves.append(steps)

        print(first_solves)
        print(math.lcm(*first_solves))


if __name__ == "__main__":

    solve_a("real_input.txt")
    solve_b("real_input.txt")



