# AOC2023

Python Advent of Code 2023 solutions  

Run ```python solve.py``` in each directory.  

input_real.txt is the full puzzle input  

test_a and test_b are the test inputs for parts a and b