def calculate_next_modifier(seq, total):

    # Keeps going until sec is length 1, can be optimised by checking for sec all 0 instead
    if len(seq) < 2:
        return total

    sub_seq = [seq[i]-seq[i-1] for i in range(1, len(seq))]

    return calculate_next_modifier(sub_seq, total+sub_seq[-1])

def calculate_first_modifier(seq):

    if len(seq) < 2:
        return seq[0]

    sub_seq = [seq[i]-seq[i-1] for i in range(1, len(seq))]

    return seq[0] - calculate_first_modifier(sub_seq)

def solve_a(input_file):

    with open(input_file, "r") as inputfile:

        total = 0

        lines = [x.strip() for x in inputfile.readlines()]
        for line in lines:
            seq = [int(x) for x in line.split(" ")]
            # print(seq)
            total += (seq[-1] + calculate_next_modifier(seq, 0))

        print(total)


def solve_b(input_file):
    with open(input_file, "r") as inputfile:
        total = 0

        lines = [x.strip() for x in inputfile.readlines()]
        for line in lines:
            seq = [int(x) for x in line.split(" ")]
            # print(seq)
            total += calculate_first_modifier(seq)

        print(total)

if __name__ == "__main__":

    solve_a("real_input.txt")
    solve_b("real_input.txt")
