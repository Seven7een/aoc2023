
MAX_MAP = 6
raw_maps = []
seeds = []

# [[_dst, _src, _range], ...]
seed_to_soil_map = []
soil_to_fertilizer_map = []
fertilizer_to_water_map = []
water_to_light_map = []
light_to_temperature_map = []
temperature_to_humidity_map = []
humidity_to_location_map = []

maps = [
    seed_to_soil_map,
    soil_to_fertilizer_map,
    fertilizer_to_water_map,
    water_to_light_map,
    light_to_temperature_map,
    temperature_to_humidity_map,
    humidity_to_location_map,
]

def solve_a():

    final_locations = {}

    for seed in seeds:
        final_locations[seed_to_loc(seed, maps)] = seed
    
    # print(final_locations)
    print(f"The minimum for part A is: {min(final_locations.keys())}")

def parsemap(_raw_map, _map):

    _raw_map = _raw_map.split("\n")[1:]

    for _vals in _raw_map:

        _dst, _src, _range = [int(x) for x in _vals.split()]

        _map.append([_dst, _src, _range])
        # print(_dst, _src, _range)

    # print(_map)

    return

def seed_to_loc(seed, maps):

    # seed

    loc = 0+seed

    # print(seed, end="")

    for _map in maps:
        for _entry in _map:
            # entry is of shape [_dst, _src, _range]
            _dst = _entry[0]
            _src = _entry[1]
            _range = _entry[2]
            if (loc >= _src) and (loc < (_src + _range)):
                loc = _dst + (loc - _src)
                break
        # print(f"->{loc}", end="")

    # print()

    return loc

def convert_seed_range(seed_ranges, map_index):

    converted_ranges = []
    remaining_ranges = seed_ranges


    if map_index > MAX_MAP:
        return seed_ranges


    while (len(remaining_ranges) > 0):

        seed_range = remaining_ranges.pop()

        seed_max = seed_range[1]
        seed_min = seed_range[0]

        for _entry in maps[map_index]:

            # entry is of shape [_dst, _src, _range]
            _dst = _entry[0]
            _src = _entry[1]
            _range = _entry[2]
            
            _modifier = _dst - _src

            _map_min = _src
            _map_max = _src + _range - 1
            # print(f"Maps: {_map_min} {_map_max}")

            # Case: Full range is contained by range map
            if (seed_min >= _map_min) and (seed_max <= _map_max):
                converted_ranges.append(
                    [seed_min + _modifier, seed_max+_modifier]
                )
                seed_range = None
                break

            # Case: Upper part of range is not contained by range map
            elif (seed_min >= _map_min) and (seed_max > _map_max) and (seed_min <= _map_max):
                # print(_map_min, _map_max)
                converted_ranges.append(
                    [seed_min + _modifier, _map_max+_modifier]
                )
                remaining_ranges.append(
                    [_map_max+1, seed_max]
                )
                seed_range = None
                break
            # Case: Lower part of range is not contained by range map
            elif (seed_min < _map_min) and (seed_max <= _map_max) and (seed_max >= _map_min):
                converted_ranges.append(
                    [_map_min+_modifier, seed_max + _modifier]
                )
                remaining_ranges.append(
                    [seed_min, _map_min-1]
                )
                seed_range = None
                break
            # Case: Upper and Lower parts of range are not contained by range map
            elif (seed_min < _map_min) and (seed_max > _map_max):
                remaining_ranges.append(
                    [seed_min, _map_min-1]
                )
                remaining_ranges.append(
                    [_map_max + 1, seed_max]
                )
                converted_ranges.append(
                    [_map_min + _modifier, _map_max+_modifier]
                )
                seed_range = None
                break
            # Case: None of range is in range map
        if (seed_range != None): converted_ranges.append(seed_range)
    # print(f"New: {sorted(converted_ranges)}")
    # print(f"Remaning: {remaining_ranges}")

    return convert_seed_range(converted_ranges + remaining_ranges, map_index+1)



def solve_b():

    seed_ranges = []

    for j in range(0, len(seeds), 2):
        
        seed_ranges.append([seeds[j], seeds[j]+seeds[j+1]-1])

    # print(seed_ranges)
    
    final_ranges = convert_seed_range(seed_ranges, 0)
    lower_numbers = [int(_pair[0]) for _pair in final_ranges]
    _min = min(lower_numbers)

    print(f"The minimum for part B is: {_min}")

if __name__ == "__main__":

    with open("real_input.txt", "r") as inputfile:

        raw_maps = [x for x in inputfile.read().split("\n\n")]

        seeds = [int(x) for x in raw_maps[0].split(":")[1].split()]

        # print(seeds)

        for i in range(1, len(maps)+1):
            parsemap(raw_maps[i], maps[i-1])

    # print(maps)

    solve_a()
    solve_b()



