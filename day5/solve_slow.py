from collections import defaultdict


# class IntRangeDict(dict):
#     def __getitem__(self, item):
#         if not isinstance(item, range):
#             for key in self:
#                 if item in key:
#                     return self[key]
#             return -1
#         else:
#             return super().__getitem__(item)

NUM_MAPS = 7

def solve_a(input_file):

    with open(input_file, "r") as inputfile:

        raw_maps = [x for x in inputfile.read().split("\n\n")]

        seeds = [int(x) for x in raw_maps[0].split(":")[1].split()]

        print(seeds)

        seed_to_soil_map = defaultdict(lambda: -1)
        soil_to_fertilizer_map = defaultdict(lambda: -1)
        fertilizer_to_water_map = defaultdict(lambda: -1)
        water_to_light_map = defaultdict(lambda: -1)
        light_to_temperature_map = defaultdict(lambda: -1)
        temperature_to_humidity_map = defaultdict(lambda: -1)
        humidity_to_location_map = defaultdict(lambda: -1)

        final_locations = {}

        maps = [
            seed_to_soil_map,
            soil_to_fertilizer_map,
            fertilizer_to_water_map,
            water_to_light_map,
            light_to_temperature_map,
            temperature_to_humidity_map,
            humidity_to_location_map,
        ]

        for i in range(1, len(maps)+1):
            parsemap(raw_maps[i], maps[i-1])


        for seed in seeds:
            final_locations[seed_to_loc(seed, maps)] = seed
        
        # print(final_locations)
        print(min(final_locations.keys()))

        # lines = [x.strip() for x in inputfile.readlines()]
        # for line in lines:
        #     print(line)

def parsemap(_raw_map, _map):

    _raw_map = _raw_map.split("\n")[1:]

    for _vals in _raw_map:

        _dst, _src, _range = [int(x) for x in _vals.split()]

        for i in range(_range):
            _map[_src+i] = _dst+i
        # print(_dst, _src, _range)

    print(_map)

    return

def seed_to_loc(seed, maps):

    loc = 0+seed

    print(seed, end="")

    for _map in maps:
        nextval = _map[loc]
        
        if nextval > -1:
            loc = 0 + nextval
        print(f"-> {loc}", end="")
    print()
    return loc

def solve_b(input_file):

    with open(input_file, "r") as inputfile:

        lines = [x.strip() for x in inputfile.readlines()]
        for line in lines:
            print(line)

if __name__ == "__main__":

    solve_a("test_a.txt")
    # solve_b("test_b.txt")



