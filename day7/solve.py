from collections import defaultdict

pts = {
    "A": 14,
    "K": 13,
    "Q": 12,
    "J": 11,
    "T": 10,
    "9": 9,
    "8": 8,
    "7": 7,
    "6": 6,
    "5": 5,
    "4": 4,
    "3": 3,
    "2": 2,
}

pts_joker = {
    "A": 13,
    "K": 12,
    "Q": 11,
    "T": 10,
    "9": 9,
    "8": 8,
    "7": 7,
    "6": 6,
    "5": 5,
    "4": 4,
    "3": 3,
    "2": 2,
    "J": 1
}

def get_rank_joker(hand):

    num_of_each_card = defaultdict(int)

    for card in hand:
        num_of_each_card[card] += 1

    amounts = list(num_of_each_card.values())
    num_jokers = hand.count("J")

    if 5 in amounts:
        return 6 # 5 of a kind
    if 4 in amounts:
        if "J" in hand:
            # Joker makes it 5 of a kind
            return 6
        return 5 # 4 of a kind
    if 3 in amounts and 2 in amounts:
        if "J" in hand:
            # 1 Joker makes it 4 of a kind, 2 or more jokers make it a 5 of a kind
            return min(4 + num_jokers, 6)
        return 4 # Full house
    if 3 in amounts:
        if "J" in hand:
            # Cannot be (3J, 2X) or (3X, 2J) or would have triggered previous if statement
            # Therefore is max (3J, 1X, 1Y) or (3X, 1J, 1Y) = 4 of a kind
            # Joker in hand can make it 4 of a kind or full-house, but 4 kind is better
            return 5
        return 3 # Three of a kind
    if amounts.count(2) > 1:
        if "J" in hand:
            # Same logic as above means best option is (2J, 2X, 1Y) = 4 of a kind
            # Second-best option is (2X, 2Y, 1J) = Full house
            # CANNOT be more than 2 jokers
            return min(3+num_jokers, 5)
        return 2 # Two pair
    if 2 in amounts:
        if "J" in hand:
            # Joker in hand can make it 2 pair if (2X, 1J, 1Y, 1Z) or 3 of a kind, but 3 of a kind is better
            return 3
        return 1 # Pair

    if (num_jokers > 0):
        return 1 # Pair

    return 0
def get_rank(hand):

    num_of_each_card = defaultdict(int)

    for card in hand:
        num_of_each_card[card] += 1

    amounts = list(num_of_each_card.values())

    # print(amounts)

    if 5 in amounts:
        return 6
    if 4 in amounts:
        return 5
    if 3 in amounts and 2 in amounts:
        return 4
    if 3 in amounts:
        return 3
    if amounts.count(2) > 1:
        return 2
    if 2 in amounts:
        return 1

    return 0

def stronger_hand(hand_1, hand_2, joker: bool):
    """ Returns 0 if hand_1 is stronger or equal, 1 otherwise"""

    if joker:
        pts_to_use = pts_joker
        get_rank_to_use = get_rank_joker
    else:
        pts_to_use = pts
        get_rank_to_use = get_rank

    # Try rank first
    hand1_rank = get_rank_to_use(hand_1)
    hand2_rank = get_rank_to_use(hand_2)

    if hand1_rank > hand2_rank:
        return 0
    if hand2_rank > hand1_rank:
        return 1

    for i in range(5):
        card_1 = pts_to_use[hand_1[i]]
        card_2 = pts_to_use[hand_2[i]]

        if card_1 > card_2:
            return 0
        if card_2 > card_1:
            return 1

    return 0

def insert_in_place(hand_text, final_hands, joker: bool):
    """
    In-place inserts the hand from hand_text into its place by power in the current final_hands list
    final_hands has shape [[hand, bet]...]
    """
    hand = hand_text.split(" ")[0]
    bet = int(hand_text.split(" ")[1])

    if len(final_hands) == 0:
        final_hands.append([hand, bet])
        return

    for i in range(len(final_hands)):
        if stronger_hand(hand, final_hands[i][0], joker) == 1:
            final_hands.insert(i, [hand, bet])
            return

    final_hands.append([hand, bet])

def solve_a(input_file):

    final_hands = []

    with open(input_file, "r") as inputfile:

        lines = [x.strip() for x in inputfile.readlines()]
        for line in lines:
            insert_in_place(line, final_hands, False)
            # print(final_hands)

        total = 0
        for i in range(0, len(final_hands)):
            total += (final_hands[i][1] * (i+1))

        print(final_hands)
        print(f"Total: {total}")


def solve_b(input_file):
    final_hands = []

    with open(input_file, "r") as inputfile:

        lines = [x.strip() for x in inputfile.readlines()]
        for line in lines:
            insert_in_place(line, final_hands, True)
            # print(final_hands)

        total = 0
        for i in range(0, len(final_hands)):
            total += (final_hands[i][1] * (i + 1))

        print(final_hands)
        print(f"Total: {total}")

if __name__ == "__main__":
    solve_a("real_input.txt")
    solve_b("real_input.txt")



