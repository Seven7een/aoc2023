import copy

processed_cards = 0

def solve_a(input_file):

    with open(input_file, "r") as inputfile:

        lines = [x.strip() for x in inputfile.readlines()]
        
        points = 0
        
        for line in lines:
            points += calculate_points(line)

        print(f"Points for total of scratch cards: {points}")


def calculate_points(line):

    winning_numbers = [int(x.strip()) for x in (line.split(":")[1].split("|")[0].split())]
    have_numbers = [int(x.strip()) for x in (line.split(":")[1].split("|")[1].split())]

    points = 0

    number_correct = len([x for x in winning_numbers if x in have_numbers])

    if (number_correct == 0): return 0

    return 2**(number_correct-1)

def calculate_matching_numbers(line):

    winning_numbers = [int(x.strip()) for x in (line.split(":")[1].split("|")[0].split())]
    have_numbers = [int(x.strip()) for x in (line.split(":")[1].split("|")[1].split())]

    points = 0

    number_correct = len([x for x in winning_numbers if x in have_numbers])

    return number_correct

def add_new_scratchcards(card, original_scratchcards, gained_scratchcards):
    card_number = int(card.split(":")[0].split()[1])
    matching_numbers = calculate_matching_numbers(card)

    if (matching_numbers == 0): return

    # print(card_number, matching_numbers, card_number+matching_numbers)

    for x in range(card_number, card_number+matching_numbers):
        # print(f"Adding: {original_scratchcards[x]}")
        gained_scratchcards.append(copy.deepcopy(original_scratchcards[x]))

def solve_b(input_file):

    processed_cards = 0

    with open(input_file, "r") as inputfile:

        original_scratchcards = [x.strip() for x in inputfile.readlines()]

        gained_scratchcards = []

        for x in range(len(original_scratchcards)):
            add_new_scratchcards(original_scratchcards[x], original_scratchcards, gained_scratchcards)
            processed_cards += 1

        while (len(gained_scratchcards) > 0):
            add_new_scratchcards(gained_scratchcards.pop(), original_scratchcards, gained_scratchcards)
            processed_cards += 1

    print(f"Total number of scratch-cards processed: {processed_cards}")


if __name__ == "__main__":

    solve_a("real_input.txt")
    print("This can take a few seconds...")
    solve_b("real_input.txt")



