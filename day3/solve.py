from collections import defaultdict
import math

digits = [str(x) for x in list(range(0,10))]
# Gear xy coords -> adjacent nums
# { xy: [x1y1, x2y2...]}
possible_gears = defaultdict(list)

def solve_a(input_file):

    with open(input_file, "r") as inputfile:

        part_numbers = []
        # An array of arrays [x,y] that are the co-ordinates of each digit of the number
        current_number = []

        lines = [x.strip() for x in inputfile.readlines()]
        for i in range(len(lines)):
            for j in range(len(lines[i])):
                _char = lines[i][j]

                if _char not in digits:
                    # # adjacency check
                    if current_number != [] and has_adjacent(lines, current_number):
                        part_numbers.append(int(build_number(lines, current_number)))
                    current_number = []

                if _char in digits:
                    current_number.append([i, j])
            
        # print(f"Part numbers: {part_numbers}")
        print(f"Part a sum: {sum(part_numbers)}")


def build_number(lines, digits):

    whole_number = ""

    for digit in digits:
        whole_number += lines[digit[0]][digit[1]]

    return int(whole_number)

def has_adjacent(lines, char_coords):

    for _char in char_coords:
        i = _char[0]
        j = _char[1]

        neighbours = []

        for new_i in range(i-1, i+2):
            for new_j in range(j-1, j+2):
                # ArrayIndex OOB check
                if new_i > -1 and new_i < len(lines) and new_j > -1 and new_j < len(lines[new_i]):
                    neighbour = lines[new_i][new_j]
                    neighbours.append(neighbour)
                    if neighbour not in (digits + ["."]):
                        # print("Has adjacent: " + lines[i][j] + " -> " + neighbour)
                        return True

    return False

def solve_b(input_file):

    with open(input_file, "r") as inputfile:

        part_numbers = []
        # An array of arrays [x,y] that are the co-ordinates of each digit of the number
        current_number = []

        lines = [x.strip() for x in inputfile.readlines()]
        for i in range(len(lines)):
            for j in range(len(lines[i])):
                _char = lines[i][j]

                if _char not in digits:
                    # # adjacency check
                    if current_number != []:
                        find_gears(lines, current_number)
                    current_number = []

                if _char in digits:
                    current_number.append([i, j])
            
        # print(f"{possible_gears}")
        sum_gear_ratios(lines)


def build_number(lines, digits):

    whole_number = ""

    for digit in digits:
        whole_number += lines[digit[0]][digit[1]]

    return int(whole_number)

def find_gears(lines, char_coords):

    for _char in char_coords:
        i = _char[0]
        j = _char[1]

        neighbours = []

        for new_i in range(i-1, i+2):
            for new_j in range(j-1, j+2):
                # ArrayIndex OOB check
                if new_i > -1 and new_i < len(lines) and new_j > -1 and new_j < len(lines[new_i]):
                    neighbour = lines[new_i][new_j]
                    neighbours.append(neighbour)
                    if neighbour == '*':
                        # print(char_coords)
                        adjacents = possible_gears[f"{(new_i, new_j)}"]
                        adjacents.append(char_coords) if char_coords not in adjacents else adjacents

def sum_gear_ratios(lines):
    gear_ratios = []

    for key in list(possible_gears.keys()):
        vals = list(possible_gears[key])
        # print(vals)
        if len(vals) == 2:
            numbers = []
            # Convert to numbers from co-ord representation
            for val in vals:
                numbers.append(build_number(lines, val))
            gear_ratios.append(math.prod(numbers))

    # print(f"Gear ratios: {gear_ratios}")
    print(f"Sum of gear ratios: {sum(gear_ratios)}")

    return gear_ratios
if __name__ == "__main__":

    solve_a("input_real.txt")
    solve_b("input_real.txt")



