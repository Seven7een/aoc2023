# Tiles is type_of_tile: ((dx, dy), (dx1, dy1))... for the connections it makes
# E.g. if a tile '|': (-1, 0), (1, 0) is on (1,1) then using the dx, dy we know connected tiles are
# (1-1, 1-0) and (1+1, 1+0) = (0,1) and (2, 1)
tiles = {
    '|': ((-1, 0), (1, 0)), # is a vertical pipe connecting north and south.
    '-': ((0, 1), (0, -1)), # is a horizontal pipe connecting east and west.
    'L': ((-1, 0), (0, 1)), # is a 90-degree bend connecting north and east.
    'J': ((-1, 0), (0, -1)), # is a 90-degree bend connecting north and west.
    '7': ((1, 0), (0, -1)), # is a 90-degree bend connecting south and west.
    'F': ((1, 0), (0, 1)), # is a 90-degree bend connecting south and east.
    '.': ((0, 0), (0, 0)), # is ground; there is no pipe in this tile.
}

# Also 'S' - the starting point, but under S another tile in tiles exists
# We use the compute_pipe_type() function to calculate that lower tile


def solve_a(input_file):

    # Shape of maze is a 2d array

    maze = []
    start = ()

    with open(input_file, "r") as inputfile:

        lines = [x.strip() for x in inputfile.readlines()]
        for line in lines:
            # print(line)
            if 'S' in line:
                start = (len(maze), line.index('S'))
            maze.append([x for x in line])

    # Compute the type of pipe under the 'S' character and replace it with proper pipe while storing starting coords
    maze[start[0]][start[1]] = compute_pipe_type(start, maze)
    result = {}
    print(f"Start is: {start}")
    traverse(start, maze, result)
    print(f"Max: {max(list(result.values()))} at {max(result, key=result.get)}")
    solve_b(maze, result)


def get_connections(location, maze, simulated_tile=""):
    """" Returns the two tiles reachable from current tile by rule of the pipe on it """
    i = location[0]
    j = location[1]

    pipe_type = maze[i][j]
    if simulated_tile != "":
        # Used in the "what if" scenario to determine what's under a starting tile
        pipe_type = simulated_tile

    changes = tiles[pipe_type]
    connections = []

    for change in changes:
        new_connection = (i + change[0], j + change[1])
        new_i = new_connection[0]
        new_j = new_connection[1]
        # Check that you're not led beyond the boundary of the maze
        if -1 < new_i < len(maze) and -1 < new_j < len(maze[new_i]):
            connections.append(new_connection)

    return connections


def compute_pipe_type(location, maze):
    """" Takes a location tuple (x, y) and returns the type of pipe that must be there based on surroundings """

    i = location[0]
    j = location[1]
    neighbours = []
    neighbour_coords = []

    pipe_type = ""

    # Get up to eight neighbours
    for new_i in range(i - 1, i + 2):
        for new_j in range(j - 1, j + 2):
            # ArrayIndex OOB check
            if -1 < new_i < len(maze) and -1 < new_j < len(maze[new_i]) and (new_i, new_j) != location:
                neighbour = maze[new_i][new_j]
                neighbour_coords.append((new_i, new_j))
                neighbours.append(neighbour)

    # Try each tile type that could be at the location of 'S'
    # Get the tiles that would be possible to go to if 'S' were of that pipe type
    # If found 2 "true connections" (S can go to tile and tile can go back to S) we've found the real type of tile 'S'
    for tile_type in list(tiles.keys()):
        my_connections = get_connections(location, maze, tile_type)
        true_connection_changes = []
        for i in range(len(neighbour_coords)):
            neighbour_connections = get_connections(neighbour_coords[i], maze)
            if location in neighbour_connections and neighbour_coords[i] in my_connections:
                true_connection_changes.append(neighbour_coords[i])

        if len(true_connection_changes) == 2:
            # print(f"Connections for {location}: {true_connection_changes}")
            print(f"Starting tile is: {tile_type}")
            return tile_type


def traverse(start, maze, result):
    """ BFS from starting point, storing depth+1 at each new set of nodes created by get_connections """
    connections_to_explore = [start]
    result[start] = 0

    while len(connections_to_explore) > 0:
        # print(f"exceptions {[list(y) for y in list(result.keys())]}")
        cte = connections_to_explore.pop()

        for new_con in [x for x in get_connections(cte, maze) if x not in result]:
            result[new_con] = result[cte] + 1
            connections_to_explore.insert(0, new_con)


# Tiles which wen passing over change the state of tiles to their right to inside/outside maze
impasse_tiles = {'L', 'J', '-'}


def solve_b(maze, loop_tiles):

    total_enclosed = 0

    for i in range(len(maze)):
        for j in range(len(maze[i])):
            if (i, j) not in loop_tiles:
                number_crossed_walls = len([(i, internal_j) for internal_j in range(j) if (i, internal_j) in loop_tiles and maze[i][internal_j] not in impasse_tiles])
                total_enclosed += number_crossed_walls % 2

    print(f"Total contained: {total_enclosed}")

if __name__ == "__main__":

    solve_a("real_input.txt")
    # solve_a("real_input.txt")



