# Tiles is type_of_tile: [[dx, dy], [dx1, dy1]]... for the connections it makes
# E.g. if a tile '|': [-1, 0], [1, 0] is on [1,1] then using the dx, dy we know connected tiles are
# [1-1, 1-0] and [1+1, 1+0] = [0,1] and [2, 1]
tiles = {
    '|': [[-1, 0], [1, 0]], # is a vertical pipe connecting north and south.
    '-': [[0, 1], [0, -1]], # is a horizontal pipe connecting east and west.
    'L': [[-1, 0], [0, 1]], # is a 90-degree bend connecting north and east.
    'J': [[-1, 0], [0, -1]], # is a 90-degree bend connecting north and west.
    '7': [[1, 0], [0, -1]], # is a 90-degree bend connecting south and west.
    'F': [[1, 0], [0, 1]], # is a 90-degree bend connecting south and east.
    '.': [[0, 0], [0, 0]], # is ground; there is no pipe in this tile.
}

# Also 'S' - the starting point, but under S another tile in tiles exists
# We use the compute_pipe_type() function to calculate that lower tile


def solve_a(input_file):

    # Shape of maze is a 2d array with elements
    # Each element has two keys
    # connections: [[x, y], [x1, y1]] = the connected elements you can go to
    # distance: int = distance from the start

    maze = []
    start = []

    with open(input_file, "r") as inputfile:

        lines = [x.strip() for x in inputfile.readlines()]
        for line in lines:
            # print(line)
            if 'S' in line:
                start = [len(maze), line.index('S')]
            maze.append([x for x in line])

    maze[start[0]][start[1]] = compute_pipe_type(start, maze)

    result = {}
    print(f"Start is: {start}")
    traverse(start, maze, result, 0)
    # print(maze)
    # print(result)
    print(f"Max: {max(list(result.values()))} at {max(result, key=result.get)}")

    solve_b(input_file, maze, [list(x) for x in result.keys()])


def get_connections(location, maze, simulated_tile=""):
    i = location[0]
    j = location[1]

    pipe_type = maze[i][j]
    if simulated_tile != "":
        # Used in the "what if" scenario to determine what's under a starting tile
        pipe_type = simulated_tile

    changes = tiles[pipe_type]
    connections = []

    for change in changes:
        new_connection = [i + change[0], j + change[1]]
        new_i = new_connection[0]
        new_j = new_connection[1]
        if -1 < new_i < len(maze) and -1 < new_j < len(maze[new_i]):
            connections.append(new_connection)

    return connections


def compute_pipe_type(location, maze):
    """" Takes a location list [x, y] and returns the type of pipe that must be there based on surroundings """

    i = location[0]
    j = location[1]
    neighbours = []
    neighbour_coords = []

    pipe_type = ""

    for new_i in range(i - 1, i + 2):
        for new_j in range(j - 1, j + 2):
            # ArrayIndex OOB check
            if -1 < new_i < len(maze) and -1 < new_j < len(maze[new_i]) and [new_i, new_j] != location:
                neighbour = maze[new_i][new_j]
                neighbour_coords.append([new_i, new_j])
                neighbours.append(neighbour)

    for tile_type in list(tiles.keys()):
        my_connections = get_connections(location, maze, tile_type)
        true_connection_changes = []
        for i in range(len(neighbour_coords)):
            neighbour_connections = get_connections(neighbour_coords[i], maze)
            if location in neighbour_connections and neighbour_coords[i] in my_connections:
                true_connection_changes.append(neighbour_coords[i])

        if len(true_connection_changes) == 2:
            # print(f"Connections for {location}: {true_connection_changes}")
            print(f"Starting tile is: {tile_type}")
            return tile_type


def traverse(start, maze, result, depth):
    """ Take a starting point, a maze and a result dictionary in which to store distances and return the dict filled """
    connections_to_explore = [start]
    result[tuple(start)] = depth

    while len(connections_to_explore) > 0:
        # print(f"exceptions {[list(y) for y in list(result.keys())]}")
        cte = connections_to_explore.pop()

        for new_con in [x for x in get_connections(cte, maze) if x not in [list(y) for y in list(result.keys())]]:
            result[tuple(new_con)] = result[tuple(cte)] + 1
            connections_to_explore.insert(0, new_con)


impasse_tiles = {'L', 'J', '-'}


def solve_b(input_file, maze, loop_tiles):

    total_enclosed = 0

    for i in range(len(maze)):
        for j in range(len(maze[i])):
            if [i, j] not in loop_tiles:
                number_crossed_walls = len([[i, internal_j] for internal_j in range(j) if [i, internal_j] in loop_tiles and maze[i][internal_j] not in impasse_tiles])
                total_enclosed += number_crossed_walls % 2
                # passable_tiles = {
                #     'h': ['-'],
                #     'v': ['|']
                # }
                # impassible_horizontal_left = 0
                # impassible_vertical_up = 0
                # impassible_vertical_down = 0
                # impassible_horizontal_right = 0
                # for internal_i in range(0, i):
                #     co_ords = [internal_i, j]
                #     if co_ords in loop_tiles and maze[internal_i][j] not in passable_tiles['v']:
                #         impassible_vertical_up += 1
                # for internal_i in range(i+1, len(maze)):
                #     co_ords = [internal_i, j]
                #     if co_ords in loop_tiles and maze[internal_i][j] not in passable_tiles['v']:
                #         impassible_vertical_down += 1
                # for internal_j in range(0, j):
                #     co_ords = [i, internal_j]
                #     if co_ords in loop_tiles and maze[i][internal_j] not in passable_tiles['h']:
                #         impassible_horizontal_left += 1
                # for internal_j in range(j+1, len(maze[i])):
                #     co_ords = [i, internal_j]
                #     if co_ords in loop_tiles and maze[i][internal_j] not in passable_tiles['h']:
                #         impassible_horizontal_right += 1
                # print(f"For: {[i, j]} Up: {impassible_vertical_up} Down:{impassible_vertical_down} Left:{impassible_horizontal_left} Right:{impassible_horizontal_right}")
                # if all([x % 2 != 0 for x in [impassible_horizontal_right, impassible_horizontal_left, impassible_vertical_up, impassible_vertical_down]]):
                #     total_enclosed += 1

    print(f"Total contained: {total_enclosed}")

if __name__ == "__main__":

    solve_a("real_input.txt")
    # solve_a("real_input.txt")



