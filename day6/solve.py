def solve_a(input_file):

    with open(input_file, "r") as inputfile:

        lines = [x.strip() for x in inputfile.readlines()]
        times = [int(x.strip()) for x in lines[0].split(":")[1].split()]
        distances = [int(x.strip()) for x in lines[1].split(":")[1].split()]

        print(times, distances)
        num_races = len(times)

        product_of_wins = 1
        for i in range(num_races):
            product_of_wins *= ways_to_win([distances[i], times[i]])

        print(f"Product of winning times: {product_of_wins}")

def ways_to_win(race: [int]) -> int:

    # race has shape [winning_distance, duration]
    winning_distance = race[0]
    duration = race[1]

    min_secs = 0
    max_secs = duration
    pts = 0

    while pts <= winning_distance and min_secs < duration:
        min_secs += 1
        pts = calculate_distance(0, 1, min_secs, duration)

    pts = 0

    while pts <= winning_distance and max_secs > 0:
        max_secs -= 1
        pts = calculate_distance(0, 1, max_secs, duration)

    return (max_secs-min_secs) + 1

def calculate_distance(starting_speed, accel, hold_secs, duration):
    speed = starting_speed + (accel * hold_secs)
    return speed * (duration - hold_secs)

def solve_b(input_file):
    with open(input_file, "r") as inputfile:
        lines = [x.strip() for x in inputfile.readlines()]
        time = int("".join([x.strip() for x in lines[0].split(":")[1].split()]))
        distance = int("".join([x.strip() for x in lines[1].split(":")[1].split()]))

        print(f"Ways to win the single race for part b: {ways_to_win([distance, time])}")

if __name__ == "__main__":

    solve_a("real_input.txt")
    solve_b("real_input.txt")



