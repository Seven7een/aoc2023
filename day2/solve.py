def solve_a(input_file):

    with open(input_file, "r") as inputfile:

        total = 0

        lines = [x.strip() for x in inputfile.readlines()]
        for line in lines:
            possible, game_id = game_possible(line, 12, 13, 14)
            if(possible):
                total += int(game_id)

        print(f"Part a total: {total}")

    return

def game_possible(game, red_max, green_max, blue_max):

    game_index = game.split(":")[0].split(" ")[1]

    # print("Game index: " + game_index)

    rounds = game.split(":")[1].split(";")

    for round in rounds:

        amounts = {
        "red": 0,
        "blue": 0,
        "green": 0
        }

        colours = [x.strip() for x in round.split(",")]
        for colour in colours:
            val = colour.split(" ")[0]
            key = colour.split(" ")[1]
            amounts[key] += int(val)

        # print(amounts)

        if amounts["red"] > red_max or amounts["blue"] > blue_max or amounts["green"] > green_max: return [False, game_index]
    return [True, game_index]

def game_min(game):

    amounts_min = {
        "red": 0,
        "blue": 0,
        "green": 0
    }

    game_index = game.split(":")[0].split(" ")[1]

    # print("Game index: " + game_index)

    rounds = game.split(":")[1].split(";")

    for round in rounds:

        amounts = {
        "red": 0,
        "blue": 0,
        "green": 0
        }

        colours = [x.strip() for x in round.split(",")]
        for colour in colours:
            val = int(colour.split(" ")[0])
            key = colour.split(" ")[1]
            
            amounts_min[key] = max(amounts_min[key], val)

    # print(amounts_min)

    power = 1
    for amount in list(amounts_min.values()):
        power *= amount

    return power

def solve_b(input_file):

    with open(input_file, "r") as inputfile:

        total = 0

        lines = [x.strip() for x in inputfile.readlines()]
        for line in lines:
            total += game_min(line)

    print(f"Part b total: {total}")

if __name__ == "__main__":

    solve_a("input_real.txt")
    solve_b("input_real.txt")


