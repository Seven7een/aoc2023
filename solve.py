def solve_a(input_file):

    with open(input_file, "r") as inputfile:

        lines = [x.strip() for x in inputfile.readlines()]
        for line in lines:
            print(line)


def solve_b(input_file):

    with open(input_file, "r") as inputfile:

        lines = [x.strip() for x in inputfile.readlines()]
        for line in lines:
            print(line)

if __name__ == "__main__":

    solve_a("test_a.txt")
    solve_b("test_b.txt")



