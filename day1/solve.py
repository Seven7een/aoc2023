digits = ["0","1","2","3","4","5","6","7","8","9"]
digit_words = {
    "zero": "0",
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven":"7",
    "eight":"8",
    "nine":"9"
}

def solve_a(input_file):

    with open(input_file, "r") as inputfile:

        total = 0

        lines = [x.strip() for x in inputfile.readlines()]
        for line in lines:
            total += find_first_and_last_digit(line)

        print(f"Part a total: {total}")



    return

def find_last_digit_complex(input_str):

    input_str = input_str[::-1]

    first_digit = ""

    for i in range(len(input_str)):
        if input_str[i] in digits:
            first_digit = input_str[i]
            return first_digit

        word = input_str[i]
        for j in range(i+1, len(input_str)):
            word = input_str[j] + word
            #print(word)
            if word in list(digit_words.keys()):
                first_digit = digit_words[word]
                return(first_digit)



    return first_digit



def find_digit_complex(input_str):

    first_digit = ""

    for i in range(len(input_str)):
        if input_str[i] in digits:
            first_digit = input_str[i]
            return first_digit

        word = input_str[i]
        for j in range(i+1, len(input_str)):
            word += input_str[j]
            # print(word)
            if word in list(digit_words.keys()):
                first_digit = digit_words[word]
                return(first_digit)



    return first_digit



def find_first_and_last_digit(inp_str):

    first_digit = ""
    last_digit = ""

    for x in inp_str:
        if x in digits:
            first_digit = x
            break

    for x in inp_str[::-1]:
        if x in digits:
            last_digit = x
            break

    return int(first_digit+last_digit)


def solve_b(input_file):

    with open(input_file, "r") as inputfile:

        total = 0

        lines = [x.strip() for x in inputfile.readlines()]
        for line in lines:
            total += int(find_digit_complex(line)+find_last_digit_complex(line))
        print(f"Part b total: {total}")

    return

if __name__ == "__main__":

    solve_a("input_real.txt")
    solve_b("input_real.txt")


